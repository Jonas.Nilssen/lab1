package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true){
            System.out.print("Let's play round "+roundCounter+"\n");
            String humanChoice = readInput(("Your choice (Rock/Paper/Scissors)?")).toLowerCase();
            String computerChoice = randomChoice();
            String choiceString= "Human chose "+humanChoice+", computer chose "+computerChoice+".";

            if (isWinner(humanChoice,computerChoice)){
                System.out.println(choiceString+" human wins!");
                humanScore +=1;
            }else if (isWinner(computerChoice,humanChoice)){
                System.out.println(choiceString+" Computer wins!");
                computerScore += 1;
            }else{
                System.out.println(choiceString+" It's a tie!");
            }
            System.out.println("Score: human " +computerScore+", computer "+humanScore);
            String continueAnswer=continuePlaying();
            if(continueAnswer.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
    }
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        while (true){
            if (rpsChoices.contains(userInput)){
                return userInput;
            }else{
                System.out.println("I don't understand"+userInput+". Try again");
                userInput = sc.next().toLowerCase();
            }
    }
    }
    public String randomChoice(){
        Random random = new Random();
        String computerChoice = rpsChoices.get(random.nextInt(rpsChoices.size()));
        return computerChoice;

    }
    public boolean isWinner(String choice1, String choice2){        
            if (choice1.equals("paper")){
                return choice2.equals("rock");
            }else if (choice1.equals("scissors")){
                    return choice2.equals("paper");
                }else{
                    return choice2.equals("scissors");
                }
        }
    
    public String continuePlaying(){
        System.out.println("Do you wish to continue playing? (y/n)?");
        String userInput = sc.next().toLowerCase();
        if (userInput.equals("no")||userInput.equals("n")){
            return "n";
        }else{
            return "y";
        }
    }

}
